package com.devandme.devandmebot.gamel;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Questions {
	
	private Langage l;
	
	public Questions(Langage l){
		this.l = l;
	}
	
	public HashMap<String, List<String>> getQuestions(){
		HashMap<String, List<String>> qer = new HashMap<String, List<String>>();
		switch (l) {
		
		case HTML:
			qer.put("À quoi correspond la balise <p>", Arrays.asList("Déclarer un paragraphe", "Déclarer une variable", "Déclarer le corps d'une page", "À la protection du code"));
			break;

		case JAVA:
			qer.put("Quel est la façon correct de déclarer une variable de type nombre", Arrays.asList("Integer nombre = 0;", "nombre Integer = 0;", "0 = (Integer)nombre;", "nombre = 0;"));
			break;
			
		default: break;
		}
		return qer;
	}

}
