package com.devandme.devandmebot.gamel;

public enum Langage {
	
	HTML("Html"),
	JAVA("Java"),
	;
	
	private String name;
	
	private Langage(String name) {
		this.name= name;
	}

	public String getName() {
		return name;
	}

}
