package com.devandme.devandmebot.gamel;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.devandme.devandmebot.utils.Functions;

import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.requests.RestAction;

public class GameLangage {
	
	private static HashMap<User, Integer> bonnesrep = new HashMap<>();
	
	public void start(User u, Langage l, TextChannel tc){
		
		//Phase de jeu
		for(Entry<String, List<String>> qar : new Questions(l).getQuestions().entrySet()){
			
			//Message "Questions"
			String messageToSend = "  **Question : **\n\n"+qar.getKey()+" ?\n";

			//Message "Reponses"
			int i = 1;
			for(String rep : Functions.randomOrder(qar.getValue())){
				messageToSend += " "+i+" - "+rep+"\n";
				i++;
			}
			
			Integer indexBonneReponse = Functions.getIndex();
			
			RestAction<Message> message = tc.sendMessage(messageToSend);
			Message mes = message.complete();
			
			waitReponse(u, mes, indexBonneReponse);
		}
		
		//Fin
		tc.sendMessage("Félicitation, vous avez obtenu "+bonnesrep.getOrDefault(u, 0)+" bonnes réponses sur "+new Questions(l).getQuestions().keySet().size()+" questions !").queue();
		
	}
	
	public void waitReponse(User u, Message mes, Integer indexBonneReponse){
		//On définit la réponse de l'utilisateur sur la bonne réponse | JUSTE POUR LE DEBUG
		Integer userReply = indexBonneReponse;
		
		if(userReply == indexBonneReponse){
			bonneReponse(u);
			mes.getTextChannel().sendMessage("Bonne réponse ! Félicitation !").queue();
		}else{
			mes.getTextChannel().sendMessage("Malheuresement ce n'est pas la bonne réponse !").queue();
		}
	}
	
	private void bonneReponse(User user){
		int i = 1;
		if(bonnesrep.containsKey(user)){
			i = bonnesrep.get(user);
			bonnesrep.remove(user);
		}
		bonnesrep.put(user, i);
	}

}
