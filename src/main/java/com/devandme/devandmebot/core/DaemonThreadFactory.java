package com.devandme.devandmebot.core;

import java.util.concurrent.ThreadFactory;

public class DaemonThreadFactory implements ThreadFactory {

    @Override
    public Thread newThread(Runnable r) {
        Thread result = new Thread(r);

        result.setDaemon(true);

        return result;
    }
}