package com.devandme.devandmebot.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PlayerDataRegistery {
	
	private static final Path PATH = Paths.get("playerdata.json");
	private static final Logger LOGGER = LoggerFactory.getLogger(PlayerDataRegistery.class);
	private static PlayerDataRegistery ourInstance = new PlayerDataRegistery();
	
	private final Map<Long, ComplexData> playerdata = new HashMap<>();
	
	private PlayerDataRegistery(){
		this.load();
	}
	
	public synchronized void load() {
        LOGGER.debug("Chargement des playerdata...");

        if(Files.exists(PATH)){
            try(BufferedReader reader = Files.newBufferedReader(PATH)){
                this.playerdata.clear();
                for (PlayerData playerdata : DevAndMe.GSON.fromJson(reader, PlayerData[].class)) {
                    this.playerdata.put(playerdata.userId, new ComplexData(playerdata.power, playerdata.xp, playerdata.messages, playerdata.muted));
                }
                LOGGER.debug("{} playerdata ont été chargés.", this.playerdata.size());
            }catch (IOException e) {
                LOGGER.error("Impossible de charger la liste des playersdata !", e);
            }
        }else {
            LOGGER.debug("Aucun playerdata chargé, le fichier est inexistant.");
        }
    }

    public synchronized void save() {
        LOGGER.debug("Sauvegarde des playerdata...");
        try (BufferedWriter writer = Files.newBufferedWriter(PATH, StandardCharsets.UTF_8)) {
            PlayerData[] playerdata = this.playerdata.entrySet().stream().map(PlayerData::new).toArray(PlayerData[]::new);
            DevAndMe.GSON.toJson(playerdata, writer);
        }catch (IOException e) {
            LOGGER.error("Impossible de charger la liste des playerdata !", e);
        }

        LOGGER.debug("{} playerdata ont été sauvegardés.", this.playerdata.size());
    }
    
    //Get & Set Data

    public boolean hasData(long userId){
    	if(this.playerdata.containsKey(userId)){
    		return true;
    	}
    	return false;
    }
    
    public void createDataIfHasnt(long userId){
    	if(!hasData(userId)){
    		this.playerdata.put(userId, new ComplexData(0, 0, 0, false));
    	}
    }
    
    public Integer getPower(long userId) {
    	createDataIfHasnt(userId);
        return this.playerdata.get(userId).getPower();
    }
    
    public void setPower(long userId, int power){
    	this.playerdata.put(userId, new ComplexData(power, getXp(userId), getMessages(userId), isMuted(userId)));
    }
    
    public Integer getXp(long userId){
    	createDataIfHasnt(userId);
    	return this.playerdata.get(userId).getXp();
    }
    
    public void addXp(long userId, int xp){
    	this.playerdata.put(userId, new ComplexData(getPower(userId), (getXp(userId)+xp), getMessages(userId), isMuted(userId)));
    }
    
    public void setXp(long userId, int xp){
    	this.playerdata.put(userId, new ComplexData(getPower(userId), xp, getMessages(userId), isMuted(userId)));
    }
    
    public int getMessages(long userId){
    	createDataIfHasnt(userId);
    	return this.playerdata.get(userId).getMessages();
    }
    
    public void addMessage(long userId, int qtt){
    	this.playerdata.put(userId, new ComplexData(getPower(userId), getXp(userId), (getMessages(userId)+qtt), isMuted(userId)));
    }
    
    public boolean isMuted(long userId){
    	createDataIfHasnt(userId);
    	return this.playerdata.get(userId).isMuted();
    }
    
    public void mute(long userId, boolean b){
    	this.playerdata.put(userId, new ComplexData(getPower(userId), getXp(userId), getMessages(userId), b));
    }

    public void resetData(long userId){
    	if(hasData(userId)){
    		this.playerdata.remove(userId);
    	}
    }
    
    //End of Data

    public static PlayerDataRegistery getInstance() {
        return ourInstance;
    }

    private final class PlayerData {

        public long userId;

        public int power, xp, messages;
        public boolean muted;

        public PlayerData(Map.Entry<Long, ComplexData> entry) {
            this.userId = entry.getKey();
            this.power = entry.getValue().getPower();
            this.xp = entry.getValue().getXp();
            this.messages = entry.getValue().getMessages();
            this.muted = entry.getValue().isMuted();
        }
    }

}
