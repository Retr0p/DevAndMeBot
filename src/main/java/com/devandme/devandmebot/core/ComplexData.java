package com.devandme.devandmebot.core;

public class ComplexData {
	
	private int power, xp, messages;
	private boolean muted;
	
	public ComplexData(int power, int xp, int messages, boolean muted) {
		this.power = power;
		this.xp = xp;
		this.messages = messages;
		this.muted = muted;
	}

	public int getPower() {
		return power;
	}

	public int getXp() {
		return xp;
	}
	
	public int getMessages(){
		return messages;
	}
	
	public boolean isMuted(){
		return muted;
	}

}
