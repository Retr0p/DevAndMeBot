package com.devandme.devandmebot.listeners;

import com.devandme.devandmebot.commands.CommandMap;
import com.devandme.devandmebot.core.DataRegistery;
import com.devandme.devandmebot.core.PlayerDataRegistery;
import com.devandme.devandmebot.utils.Functions;
import com.devandme.devandmebot.utils.RewardsChecker;
import com.devandme.devandmebot.utils.Utils;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.channel.text.TextChannelCreateEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.EventListener;
import net.dv8tion.jda.core.managers.GuildController;

import static com.devandme.devandmebot.utils.Functions.*;

public class BotListener implements EventListener {
	
	private final CommandMap commandmap;

	public BotListener(CommandMap commandmap) {
		this.commandmap = commandmap;
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof MessageReceivedEvent) onMessage((MessageReceivedEvent)event);
		else if(event instanceof GuildMemberJoinEvent) onJoin((GuildMemberJoinEvent)event);
		else if(event instanceof TextChannelCreateEvent) onChannelCreate((TextChannelCreateEvent)event);
		else if(event instanceof GuildMemberLeaveEvent) onLeave((GuildMemberLeaveEvent)event);
	}
	
	private boolean isMuted(Member m){
		for(Role r : m.getRoles()){
			if(r.getName().equalsIgnoreCase(Utils.getMutedName())){
				return true;
			}
		}
		return false;
	}

	private void onLeave(GuildMemberLeaveEvent event) {
		PlayerDataRegistery.getInstance().mute(event.getUser().getIdLong(), isMuted(event.getMember()));
	}

	private void onChannelCreate(TextChannelCreateEvent event) {
		TextChannel tc = event.getChannel();
		tc.createPermissionOverride(Utils.getMuteRole(event.getGuild())).setDeny(Permission.MESSAGE_WRITE).queue();
	}

	private void onJoin(GuildMemberJoinEvent event) {
		User user = event.getUser();
		
		if(PlayerDataRegistery.getInstance().isMuted(user.getIdLong())){
			new GuildController(event.getGuild()).addRolesToMember(event.getMember(), Utils.getMuteRole(event.getGuild())).queue();
			return;
		}
		
		PlayerDataRegistery.getInstance().setXp(user.getIdLong(), 100);
		sendPrivateMessage(user, "[Dev&Me] Info | Vous avez reçu 100 xp (Raison : Première connexion).");
		forcePrivateMessage(
				event.getGuild(), 
				user, 
				"Bienvenue sur le Discord «Dev & Me», " + user.getAsMention() + " !\n\nPour commencer, je me présente, je suis le bot officiel du serveur. Je suis là pour t'accompagner et t'aider en cas de besoin ! Si tu souhaites interagir avec moi, il suffit de se rendre dans le salon #bot-et-spam et de précéder son message par **!**. Pour afficher la liste des commandes, il suffit d'écrire : **!help**.\n\nLe serveur est spécialement conçu pour les développeurs et pour les personnes intéresser par l'univers de la programmation. Que vous soyers débutant en programmation ou expert, que vous ne maîtrisez aucun langage ou plusieurs dizaines, pas de problème, tout le monde est accepté et a une place importante dans le projet. De nombreux grades sont à votre disposition pour identifier facilement vos aptitudes. Le but principal du serveur est de passer un bon moment et de partager notre passion pour la programmation.\n\nPour le bien de tous, il faut respecter quelques règles élémentaires :\n- pas d'insultes ni de langages vulgaires (privilégiées un langage courant),\n- un minimum de langage « sms ».\nIl est également important de préciser que toute forme de recrutement (ou de tentatives) est strictement interdite et sera fortement sanctionnée. Les développeurs ne sont pas là pour être recruté, et la majorité d'entre eux ont déjà de solides projets.\n\nQuelques petites informations sur l'équipe du Discord :\n  Créateur original : Retr0.\n  Maintenu par : DiscowZombie – Mathéo [Administrateur], Arthur340 – Arthur [Administrateur], OriginalPainZ (Nallraen) – Théo [Administrateur], Sixestla (Redall) – Sixela [Modérateur], Myuto [Support]\n_Dernière mise à jour : 12 novembre 2017_\n\n\nVous avez maintenant toutes les clés pour passer un bon moment sur le Discord ! Amusez-vous bien."
				);
	}
	
	private void onMessage(MessageReceivedEvent event) {
		if(event.getAuthor().equals(event.getJDA().getSelfUser())) return;
		if(!(event.getChannel() instanceof TextChannel)) return;
		
		TextChannel tc = event.getTextChannel();
		User user = event.getAuthor();
		Member m = event.getMember();
		String message = event.getMessage().getContent();
		
		//Add message to count
		if(!message.startsWith(commandmap.getTag())){
			new DataRegistery().addMessage();
			//Add message to playercount
			if(message.length() > 5){
				PlayerDataRegistery.getInstance().addMessage(user.getIdLong(), 1);
				new RewardsChecker(user);
			}
		}
		
		if((PlayerDataRegistery.getInstance().getMessages(user.getIdLong()) <= 40) && (Functions.countForbiddenWordOccurrences(message) >= 2)){
			//Supprimer le message
			event.getMessage().delete().queue();
			//Rendre muet le joueur
			new GuildController(event.getGuild()).addRolesToMember(m, Utils.getMuteRole(event.getGuild())).queue();
			//Warn member
			forcePrivateMessage(event.getGuild(), user, "[Dev&Me] Vous avez été réduit au silence : Le recrutement est interdit sur ce Discord ! \n*Si c'est une erreur, merci de contacter Mathéo/DiscowZombie en privé.*");
			PlayerDataRegistery.getInstance().mute(user.getIdLong(), true);
			
			Utils.getStaffChannel(event.getGuild()).sendMessage("**"+user.getAsMention().toString()+"** a été réduit au silence pour **Recrutement** _("+message.toString()+")_.").queue();
			return;
		}
		
		//Only ONE MESSAGE IS ALLOW ON THIS CHANNEL !
		if(tc.getName().equalsIgnoreCase("presentation")){
			tc.createPermissionOverride(m).setDeny(Permission.MESSAGE_WRITE).queue();
			return;
		}
		
		if(message.startsWith(commandmap.getTag())){
			//Add commande to count
			new DataRegistery().addCommande();
			
			//Bon salon
			if(tc.getName().equalsIgnoreCase("bot-et-spam")){
				message = message.replaceFirst(commandmap.getTag(), "");
				commandmap.commandUser(event.getTextChannel(), event.getAuthor(), message, event.getMessage());
			}
			//Mauvais salon
			else{
				sendPrivateMessage(user, "[Dev&Me] Merci d'utiliser le salon 'bot-et-spam' pour les commandes !");
				//Supprimer le message
				if((event.getTextChannel() != null) && (event.getGuild().getSelfMember().hasPermission(Permission.MESSAGE_MANAGE))){
					event.getMessage().delete().queue();
				}
			}
		}
	
	}

}
