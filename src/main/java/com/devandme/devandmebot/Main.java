package com.devandme.devandmebot;

import com.devandme.devandmebot.core.DevAndMe;
import org.slf4j.LoggerFactory;

public final class Main {
	
	/*
	 * Pour les options de configurations poussé, voir "utils/Utils.java".
	 * Pour les messages, voir "utils/MessageManager.java".
	 */
	
    public static void main(String[] args) throws Throwable {
        try{
            new DevAndMe().loop();
        }catch (Throwable t){
            LoggerFactory.getLogger("main").error("Impossible de démarrer le bot: ", t);
            throw t;
        }
    }
}
