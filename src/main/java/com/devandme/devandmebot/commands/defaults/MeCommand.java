package com.devandme.devandmebot.commands.defaults;

import java.awt.Color;
import java.time.format.DateTimeFormatter;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.commands.CommandMap;
import com.devandme.devandmebot.core.PlayerDataRegistery;
import com.devandme.devandmebot.utils.Functions;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;

public class MeCommand {
	
	private final CommandMap commandMap;
	
	public MeCommand(CommandMap commandMap){
		this.commandMap = commandMap;
	}

	@Command(name="me", description="Afficher des informations relatives à votre compte")
	private void power(User user, MessageChannel channel, Message message, Guild guild, String[] args){
		if((message.getMember().hasPermission(Permission.MANAGE_SERVER)) && (message.getMentionedUsers() != null) && (message.getMentionedUsers().size() == 1) && (!message.getMentionedUsers().get(0).isBot())){
			//Il veux les infos sur qqun
			User cible = message.getMentionedUsers().get(0);
			displayInfos(guild, cible, guild.getMember(cible), (TextChannel)channel);
			return;
		}
		
		displayInfos(guild, user, message.getMember(), (TextChannel)channel);
	}

	private void displayInfos(Guild guild, User user, Member member, TextChannel tc) {
		PlayerDataRegistery pdr = PlayerDataRegistery.getInstance();
		Integer power = pdr.getPower(user.getIdLong());
		Integer xp = pdr.getXp(user.getIdLong());
		Integer messages = pdr.getMessages(user.getIdLong());
		
		EmbedBuilder me = new EmbedBuilder();
		me.setAuthor(user.getName(), null, user.getAvatarUrl());
	    me.setColor(Color.lightGray);
	    me.addField("Nom d'usage : ", member.getEffectiveName(), false);
	    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm - dd.MM.yyyy");
	    me.addField("Première connexion : ", member.getJoinDate().format(formatter), false);
	    me.addField((member.getRoles().size() > 1 ? "Roles : " : "Role : "), Functions.displayRoles(member.getRoles()), false);
	    me.addField("Power total : ", Integer.toString(commandMap.getTotalUserPower(guild, user)) + " (" + Integer.toString(power) + "/" + Integer.toString(commandMap.getRankUserPower(guild, user)) + ")", false);
	    me.addField("Messages : ", Integer.toString(messages), false);
	    me.addField("Experience : ", Integer.toString(xp), false);
	    
	    tc.sendMessage(me.build()).queue();
	}

}
