package com.devandme.devandmebot.commands.defaults;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.commands.Command.ExecutorType;
import com.devandme.devandmebot.core.PlayerDataRegistery;
import com.devandme.devandmebot.utils.Utils;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.managers.GuildController;

import static com.devandme.devandmebot.utils.Functions.*;

public class MuteCommand {
	
	@Command(name="mute", type=ExecutorType.USER, description="Permet de rendre silencieux", power=70)
	private void mute(Guild guild, User user, MessageChannel channel, Message message, String[] args){
		if(args.length <= 1 || message.getMentionedUsers().size() == 0){
			channel.sendMessage("mute <@user> <raison>").queue();
			return;
		}
		
		User toMute = message.getMentionedUsers().get(0);
		
		StringBuilder sb = new StringBuilder();
        for (int j = 1; j < args.length; j++) {
          sb.append(args[j]).append(" ");
        }
        String raison = sb.toString().trim();
		
        message.delete().queue();
        
        PlayerDataRegistery.getInstance().mute(toMute.getIdLong(), true);
		//Rendre muet l'utilisateur
		new GuildController(guild).addRolesToMember(new GuildController(guild).getGuild().getMember(toMute), Utils.getMuteRole(guild)).queue();
        forcePrivateMessage(guild, toMute, "[Dev&Me] Vous avez été réduit au silence : **"+raison+"**.");
        
		Utils.getStaffChannel(guild).sendMessage("**"+user.getAsMention()+"** a réduit au silence **"+toMute.getAsMention().toString()+"** pour **"+raison+"**.").queue();
	}
	
	@Command(name="unmute", type=ExecutorType.USER, description="Permet de redonner la parole", power=70)
	private void unmute(Guild guild, User user, MessageChannel channel, Message message, String[] args){
		if(message.getMentionedUsers().size() == 0){
			channel.sendMessage("unmute <@user>").queue();
			return;
		}
		
		User toUnmute = message.getMentionedUsers().get(0);
		
		if(!PlayerDataRegistery.getInstance().isMuted(toUnmute.getIdLong())){
			//Isn't muted
			channel.sendMessage("Cet utilisateur n'est pas silencieux !").queue();
			return;
		}
		
		message.delete().queue();
		
		PlayerDataRegistery.getInstance().mute(toUnmute.getIdLong(), false);
		new GuildController(guild).removeRolesFromMember(new GuildController(guild).getGuild().getMember(toUnmute), Utils.getMuteRole(guild)).queue();
		forcePrivateMessage(guild, toUnmute, "[Dev&Me] Vous avez retrouver la parole !");
		
		Utils.getStaffChannel(guild).sendMessage("**"+user.getAsMention()+"** a rédonné la parole à **"+toUnmute.getAsMention().toString()+"**.").queue();
	}

}
