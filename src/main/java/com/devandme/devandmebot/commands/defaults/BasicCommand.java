package com.devandme.devandmebot.commands.defaults;

import java.awt.Color;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.commands.Command.ExecutorType;
import com.devandme.devandmebot.commands.CommandMap;
import com.devandme.devandmebot.commands.SimpleCommand;
import com.devandme.devandmebot.core.DevAndMe;
import com.devandme.devandmebot.utils.MessageManager.MessagesList;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import static com.devandme.devandmebot.utils.MessageManager.*;

public class BasicCommand {
	
	private final DevAndMe dam;
	private final CommandMap commandMap;
	
	public BasicCommand(DevAndMe dam, CommandMap commandMap){
		this.dam = dam;
		this.commandMap = commandMap;
	}
	
	@Command(name="stop", type=ExecutorType.CONSOLE, description="Eteindre le bot")
	private void stop(){
		dam.stop();
	}
	
	@Command(name="help", type=ExecutorType.USER, description="Affiche le message d'aide")
	private void help(Guild guild, User user, MessageChannel channel){
	    
	    EmbedBuilder help = new EmbedBuilder();
	    help.setTitle(getMessage(MessagesList.CMD_HELP_LIST));
	    help.setColor(Color.orange);
	    
	    for(SimpleCommand cmd : commandMap.getCommands()){
			if(cmd.getType() == ExecutorType.CONSOLE) continue;
			if((guild == null) || (cmd.getPower() > commandMap.getTotalUserPower(guild, user))) continue;
			
			if(cmd.getPower() > 0) help.addField(cmd.getName() + " (Power: " + Integer.toString(cmd.getPower()) + ")", cmd.getDescription(), false);
			else help.addField(cmd.getName(), cmd.getDescription(), false);
		}
	    
	    channel.sendMessage(help.build()).queue();
	}
}