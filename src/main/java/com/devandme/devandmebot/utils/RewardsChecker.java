package com.devandme.devandmebot.utils;

import com.devandme.devandmebot.core.PlayerDataRegistery;

import net.dv8tion.jda.core.entities.User;

public class RewardsChecker {
	
	public RewardsChecker(User user){
		int messagesCount = PlayerDataRegistery.getInstance().getMessages(user.getIdLong());
		switch (messagesCount) {
		case 1: send(user, 10, "Votre premier message"); break;
		case 10: send(user, 10, "Votre dixième messages"); break;
		case 69: send(user, 15, "69 messages, aucun commentaire"); break;
		case 100: send(user, 20, "100 messages ! Félicitations"); break;
		case 500: send(user, 50, "Déjà 500 messages, impressionnant !"); break;
		case 1000: send(user, 120, "1000 messages, personne ne peut vous arrêter !"); break;
		case 5000: send(user, 200, "5000 messages :O"); break;
		default: break;
		}
	}
	
	private void send(User user, int xpQtt, String raison){
		PlayerDataRegistery.getInstance().addXp(user.getIdLong(), xpQtt);
		Functions.sendPrivateMessage(user, "[Dev&Me] Info | Vous avez reçu "+xpQtt+" xp (Raison : "+raison+").");
	}

}
