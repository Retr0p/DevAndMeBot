package com.devandme.devandmebot.utils;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;

public abstract class Utils {
	
	//TODO
	//A retravailler pour supporter plusieurs guilds
	
	private static String mutedName = "Muté";
	private static String staffName = "staff";
	
	/*
	 * Call in FIRST
	 */
	public static void setup(Guild guild){
		//Pour le salon Staff
		if(guild.getTextChannelsByName(staffName, true).size() == 0){
			//On créée le salon Staff s'il n'existe pas
			guild.getController().createTextChannel(staffName).queue();
		}
				
		//Pour le grade Mute
		if(guild.getRolesByName(mutedName, true).size() == 0){
			//On créé le role Muté s'il n'existe pas encore
			Role muted = guild.getController().createRole().setHoisted(false).setMentionable(false).setName(mutedName).complete();
			for(TextChannel tc : guild.getTextChannels()){
				//On interdit de parler dans tous les salons
				tc.createPermissionOverride(muted).setDeny(Permission.MESSAGE_WRITE).queue();
			}
		}
	}
	
	public static String getMutedName(){
		return mutedName;
	}
	
	public static Role getMuteRole(Guild guild){
		setup(guild);
		return guild.getRolesByName(mutedName, true).get(0);
	}
	
	public static String getStaffName(){
		return staffName;
	}
	
	public static TextChannel getStaffChannel(Guild guild){
		setup(guild);
		return guild.getTextChannelsByName(staffName, true).get(0);
	}
	
	public static Role getDefaultRole(Guild guild){
		return guild.getPublicRole();
	}

}
