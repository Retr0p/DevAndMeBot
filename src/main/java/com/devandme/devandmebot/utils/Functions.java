package com.devandme.devandmebot.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.devandme.devandmebot.gamel.Langage;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.entities.impl.UserImpl;

public abstract class Functions {
	
	public static String formatInteger(int i){
		if(i/1000000 >= 1){
			return String.format("%.3f", (double)i/1000000)+"M";
		}
		if(i/1000 >= 1){
			return String.format("%.1f", (double)i/1000)+"k";
		}
		return Integer.toString(i);
	}
	
	//On lui envoye un message à tous les coups
	public static void forcePrivateMessage(Guild guild, User user, String str){
		boolean error = sendPrivateMessage(user, str);
		
		if(error){
			//Il n'accepte pas les MP :(
			System.out.println(guild.getTextChannelsByName("logs-"+user.getIdLong(), false).size());
			if(guild.getTextChannelsByName("logs-"+user.getIdLong(), false).size() == 0){
				TextChannel tc = (TextChannel)guild.getController().createTextChannel("logs-"+user.getIdLong()).complete();
				tc.createPermissionOverride(guild.getPublicRole()).setDeny(Permission.MESSAGE_READ).queue();
				tc.createPermissionOverride(guild.getMember(user)).setAllow(Permission.MESSAGE_READ).queue();
				tc.sendMessage("*Bonjour, "+user.getAsMention()+". Il semble que vous n'acceptiez pas mes messages privés, cependant j'ai parfois besoin de vous contacté. Lorsque ça sera nécessaire, j'utiliserais ce salon spécialement créé pour nous deux. Pour être tenu informés, merci de laisser les notifications actives dans ce salon. Amusez-vous bien sur le Discord ! \n\nPS: Dès que vous accepterait mes messages privés, j'arreterais d'utiliser ce salon.*").queue();
			}
			
			TextChannel playerChannel = guild.getTextChannelsByName("logs-"+user.getIdLong(), false).get(0);
			playerChannel.sendMessage(str).queue();
		}
	}
	
	//On essaye de lui envoyer un message
	//Si ca fail, on ne fait rien (on return juste true = il y a eu une erreur)
	/*
	 * IMPORTANT : This do not work !
	 */
	public static boolean sendPrivateMessage(User user, String str){
		//On essaye d'ouvre le salon privé
		boolean error = false;
		if(!user.hasPrivateChannel()){
			try{
				user.openPrivateChannel().queue();
			}catch(Exception e){
				error = true;
			}
		}
		//On essaye lui envoye le message
		try{
			((UserImpl)user).getPrivateChannel().sendMessage(str).queue();
		}catch(Exception e){
			error = true;
		}
		return error;
	}
	
	public static String displayList(List<?> l){
		String caract = null;
		for(Object elements : l){
			if(caract == null) caract = elements.toString();
			else caract += ", " + elements.toString();
		}
		return caract;
	}
	
	public static String displayLangageList(Langage[] l){
		String caract = null;
		for(Langage lan : l){
			if(caract == null) caract = lan.getName();
			else caract += ", " + lan.getName();
		}
		return caract;
	}
	
	public static String displayRoles(List<Role> roles){
		String caract = null;
		for(Role r : roles){
			if(caract == null) caract = r.getName();
			else caract += ", " + r.getName();
		}
		return caract;
	}
	
	public static Langage checkLangageSyntax(String language){
		if(Langage.valueOf(language.toLowerCase()) != null){
			return Langage.valueOf(language.toLowerCase());
		}
		return null;
	}
	
	/*
	 * Hard-core melange - Fait maison par DiscowZombie
	 */
	public static List<String> randomOrder(List<String> ordoned){
		//l.get(0) == la bonne réponse;
		List<String> nonOrdoned = new ArrayList<>();
		Random r = new Random();
		Integer index = (r.nextInt(ordoned.size()-1))+1;
		
		nonOrdoned.addAll(ordoned);
		
		String cache = nonOrdoned.get(index);
		Integer cachePlace = index;
		String good = nonOrdoned.get(0);
		Integer goodPlace = 0;
		
		nonOrdoned.remove((int)cachePlace);
		nonOrdoned.remove((int)goodPlace);
		nonOrdoned.add(goodPlace, cache);
		nonOrdoned.add(cachePlace, good);

		setIndex(index+1);
		return nonOrdoned;
	}
	
	public static Integer getIndex() {
		return index;
	}

	public static void setIndex(Integer index) {
		Functions.index = index;
	}

	private static Integer index = 1;

	/*
	private static final Stream<Pattern> FORBIDDEN_EXPRESSIONS = Stream.of(
            "recrutement",
            "codeur",
            "developpeur",
            "dev",
            "recherche",
            "rejoindre",
            "projet",
            "cherche"
			)
			.map(str -> Pattern.compile(".*\\b(" + Pattern.quote(str) + ")\\b.*"));
	 */
	
    public static int countForbiddenWordOccurrences(String message) {
    	int i = 0;
    	for(String s : Arrays.asList("recrutement", "codeur", "developpeur", "dev", "recherche", "rejoindre", "projet", "cherche")){
    		if(message.contains(s)) i++;
    	}
    	
    	return i;
    	//return FORBIDDEN_EXPRESSIONS.filter(i -> i.matcher(message).matches()).count();
    }
	
	
}
