package com.devandme.devandmebot.utils;

public class MessageManager {
	
	public static String getMessage(MessagesList messagePath){
		return (messagePath.getMessage() != null ? messagePath.getMessage() : messagePath.toString());
	}
	
	public enum MessagesList {
		CMD_HELP_LIST("Liste des commandes :"),
		;
		
		private String message = null;
		
		MessagesList(String message){
			this.message = message;
		}
		
		public String getMessage(){
			return message;
		}
	}

}
